package org.weasis.leapmotion;
import org.weasis.leapmotion.PrintLog;
import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.net.InetAddress;
import java.util.Observable;
import java.net.*;

import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

/**
 * Created by KangShiang on 2016-01-21.
 */
class ChatSocketClient extends Observable {
    private Socket socket = null;
    private ServerSocket server_socket = null;
    private InputStream inStream = null;
    private OutputStream outStream = null;
    private InetAddress ip_address = null;
    private int port = 0;
    private String message;
    private final String ip_addr = "128.189.95.19";
    private final String port_no = "1078";
    private boolean isConnected = false;
    
    public ChatSocketClient() {
        try {
        	PrintLog.LPrint("Connecting to server");
            this.ip_address = InetAddress.getByName(ip_addr);
            this.port = Integer.parseInt(port_no);
            this.isConnected = false;
            autoConnect();
        }
        catch (UnknownHostException e){
        	PrintLog.EPrint("IP cannot be found!");
        } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    private void retryConnection() throws Exception{
    	Thread readThread = new Thread() {
            public void run() {
                while (isConnected == false) {
                    try {
                    	InetAddress ip;
                        ip = InetAddress.getLocalHost();
                        String message = ip.getHostAddress();
                        DatagramSocket clientSocket = new DatagramSocket();
                        InetAddress IPAddress = InetAddress.getByName("224.1.1.1");
                        byte[] sendData = message.getBytes();
                        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, 5007);
                        clientSocket.send(sendPacket);
                        PrintLog.DPrint("Sending IP to server");
                        clientSocket.close();
                        Thread.sleep(3000);
                    }catch (SocketException se){
                        se.printStackTrace();
                    } catch (IOException i) {
                        i.printStackTrace();
                    } catch (InterruptedException e) {
						e.printStackTrace();
					}
                }
            }
        };
        readThread.start();
    }
    
    private void autoConnect() throws Exception{
    	retryConnection();
        PrintLog.DPrint("Waiting for Leap Motion to connect");
        startTCPServer();
    }
    
    private void startTCPServer() throws Exception{
    	this.server_socket = new ServerSocket(this.port);         
    	this.createSocket();
    }
    
    public void receivedMessage(String value) {
        // mark as value changed
        setChanged();
        // trigger notification
        notifyObservers(value);
    }

    public void createSocket() {
        try {
            socket = this.server_socket.accept();
            this.isConnected = true;
            PrintLog.LPrint("Connected to Server");
            inStream = socket.getInputStream();
            outStream = socket.getOutputStream();
            createReadThread();
            createWriteThread();
        } catch (UnknownHostException u) {
            u.printStackTrace();
        } catch (IOException io) {
            io.printStackTrace();
        }
    }

    public void createReadThread() {
        Thread readThread = new Thread() {
            public void run() {
                while (socket.isConnected()) {

                    try {
                        byte[] readBuffer = new byte[200];
                        int num = inStream.read(readBuffer);

                        if (num > 0) {
                            byte[] arrayBytes = new byte[num];
                            System.arraycopy(readBuffer, 0, arrayBytes, 0, num);
                            String recvedMessage = new String(arrayBytes, "UTF-8");
                            receivedMessage(recvedMessage);
                        }/* else {
                            // notify();
                        }*/
                        ;
                        //System.arraycopy();
                    }catch (SocketException se){
                        System.exit(0);
                    } catch (IOException i) {
                        i.printStackTrace();
                    }
                }
            }
        };
        readThread.setPriority(Thread.MAX_PRIORITY);
        readThread.start();
    }

    public void createWriteThread() {
        Thread writeThread = new Thread() {
            public void run() {
                while (socket.isConnected()) {
                    try {
                        BufferedReader inputReader = new BufferedReader(new InputStreamReader(System.in));
                        sleep(100);
                        String typedMessage = inputReader.readLine();
                        if (typedMessage != null && typedMessage.length() > 0) {
                            synchronized (socket) {
                                outStream.write(typedMessage.getBytes("UTF-8"));
                                sleep(100);
                            }
                        }
                        ;
                        //System.arraycopy();
                    } catch (IOException i) {
                        i.printStackTrace();
                    } catch (InterruptedException ie) {
                        ie.printStackTrace();
                    }
                }
            }
        };
        writeThread.setPriority(Thread.MAX_PRIORITY);
        writeThread.start();
    }
}