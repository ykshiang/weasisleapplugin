/*******************************************************************************
 * Copyright (c) 2010 Nicolas Roduit.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Nicolas Roduit - initial API and implementation
 ******************************************************************************/
package org.weasis.leapmotion;

import java.awt.Color;
import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;

import org.weasis.core.ui.docking.PluginTool;

import bibliothek.gui.dock.common.CLocation;

public class LeapTool extends PluginTool {

    public static final String BUTTON_NAME = "Tool Sample";

    private final JScrollPane rootPane = new JScrollPane();
    public JFrame leapFrame;
	public JPanel leapPanel;
	
    public LeapTool(Type type) {
        super(BUTTON_NAME, "Leap Motion", type, 120);
        dockable.setTitleIcon(new ImageIcon(LeapTool.class.getResource("/icon/22x22/text-html.png"))); //$NON-NLS-1$
        
		leapPanel = new JPanel();
		leapPanel.setBackground(Color.red);
		
        rootPane.add(leapPanel);
        setDockableWidth(75);
    }
    
    @Override
    public Component getToolComponent() {
        JViewport viewPort = rootPane.getViewport();
        if (viewPort == null) {
            viewPort = new JViewport();
            rootPane.setViewport(viewPort);
        }
        if (viewPort.getView() != this) {
            viewPort.setView(this);
        }
        return rootPane;
    }

    @Override
    protected void changeToolWindowAnchor(CLocation clocation) {
        // TODO Auto-generated method stub
    }

}