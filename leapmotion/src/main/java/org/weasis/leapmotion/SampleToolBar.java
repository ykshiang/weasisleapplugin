package org.weasis.leapmotion;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import org.weasis.core.api.gui.util.JMVUtils;
import org.weasis.core.api.media.data.ImageElement;
import org.weasis.core.ui.util.WtoolBar;

public class SampleToolBar<E extends ImageElement> extends WtoolBar {
	static JButton statusButton = new JButton();
	static JButton handsButton = new JButton();
	static JButton toolsButton = new JButton();
	
	private void addStatusButton(){
		statusButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() instanceof Component) {
                }
            }
        });
        add(statusButton);
	}
	
	public static void changeStatusToGreen(){
    	statusButton.setToolTipText("Motion Detected");
    	statusButton.setIcon(new ImageIcon(SampleToolBar.class.getResource("/icon/32x32/greenLeap.png")));
    }
    
    public static void changeStatusToRed(){
    	statusButton.setToolTipText("Motion Not Detected");
    	statusButton.setIcon(new ImageIcon(SampleToolBar.class.getResource("/icon/32x32/redLeap.png")));
    }
    
    public static void changeHandsToOne(){
    	handsButton.setToolTipText("One Finger Detected");
    	handsButton.setIcon(new ImageIcon(SampleToolBar.class.getResource("/icon/32x32/oneFinger.png")));
    }
    
    public static void changeHandsToTwo(){
    	handsButton.setToolTipText("Two Fingers Detected");
    	handsButton.setIcon(new ImageIcon(SampleToolBar.class.getResource("/icon/32x32/twoFinger.png")));
    }
    
    public static void changeHandsToThree(){
    	handsButton.setToolTipText("Three Fingers Detected");
    	handsButton.setIcon(new ImageIcon(SampleToolBar.class.getResource("/icon/32x32/threeFinger.png")));
    }
    
    public static void changeHandsToFour(){
    	handsButton.setToolTipText("Four Fingers Detected");
    	handsButton.setIcon(new ImageIcon(SampleToolBar.class.getResource("/icon/32x32/fourFinger.png")));
    }
    
    public static void changeHandsToFive(){
    	handsButton.setToolTipText("Five Fingers Detected");
    	handsButton.setIcon(new ImageIcon(SampleToolBar.class.getResource("/icon/32x32/fiveFinger.png")));
    }
    
    public static void changeHandsToNo(){
    	handsButton.setToolTipText("No Finger Detected");
    	handsButton.setIcon(new ImageIcon(SampleToolBar.class.getResource("/icon/32x32/noFinger.png")));
    }
    
    public static void changeToolsToScroll(){
    	toolsButton.setToolTipText("No Finger Detected");
    	toolsButton.setIcon(new ImageIcon(SampleToolBar.class.getResource("/icon/32x32/scroll.png")));
    }
    
    public static void changeToolsToPan(){
    	toolsButton.setToolTipText("No Finger Detected");
    	toolsButton.setIcon(new ImageIcon(SampleToolBar.class.getResource("/icon/32x32/pan.png")));
    }
    public static void changeToolsToZoom(){
    	toolsButton.setToolTipText("No Finger Detected");
    	toolsButton.setIcon(new ImageIcon(SampleToolBar.class.getResource("/icon/32x32/zoom.png")));
    }
    public static void changeToolsToMeasure(){
    	toolsButton.setToolTipText("No Finger Detected");
    	toolsButton.setIcon(new ImageIcon(SampleToolBar.class.getResource("/icon/32x32/measure.png")));
    }
    public static void changeToolsToCrosshair(){
    	toolsButton.setToolTipText("No Finger Detected");
    	toolsButton.setIcon(new ImageIcon(SampleToolBar.class.getResource("/icon/32x32/crosshair.png")));
    }
	private void addHandsButton(){
		handsButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() instanceof Component) {
                }
            }
        });
        add(handsButton);
	}
	
	private void addToolsButton(){
		toolsButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() instanceof Component) {
                }
            }
        });
        add(toolsButton);
	}
	
    public SampleToolBar() {
        super("Sample Toolbar", 500);
        addStatusButton();
        addHandsButton();
        addToolsButton();
        changeStatusToRed(); //$NON-NLS-1$
        changeHandsToOne(); //$NON-NLS-1$
        changeToolsToScroll(); //$NON-NLS-1$
    }
}
