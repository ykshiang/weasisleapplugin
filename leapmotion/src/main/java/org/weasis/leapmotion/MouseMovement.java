package org.weasis.leapmotion;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.InputEvent;
import java.io.IOException;

import org.weasis.dicom.viewer2d.EventManager;

public class MouseMovement {
	
	private static final int USABLE_SPACE_Y = 300;
	private static final int USABLE_SPACE_X = 300;
	private static final int CLICK_THRESH = 0;
	
	private static float xMultiplier;
	private static float yMultiplier;
	
	public static final int SCROLL = 0; 
	public static final int ZOOM = 1; 
	public static final int PAN = 2; 
	public static final int CROSSHAIR = 3; 
	public static final int WHAT = 4; 
	
	private static Robot mouse;
	
	
	public static int isClicked; 
	public static int currentTool;
	
	public MouseMovement(){
		try {
			mouse = new Robot();
			xMultiplier = (float) Toolkit.getDefaultToolkit().getScreenSize().getWidth()/USABLE_SPACE_X;
			yMultiplier = (float) Toolkit.getDefaultToolkit().getScreenSize().getHeight()/USABLE_SPACE_Y;
			isClicked = 0;
			currentTool = SCROLL;
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void MouseHandler(String[] parts){
			PrintLog.DPrint("PredefinedHandler!!!");
	    	String[] Xval = (parts[2].split(":"));
	    	String[] Yval = (parts[3].split(":"));

	    	float X = 0, Y = 0;

	    	if(Xval[0].equals("positionX")){
	    		X = Float.parseFloat(Xval[1]);
	    	}else{
	    		PrintLog.DPrint("Error Xval[0] is not positionX");
	    	}
	    	
	    	if(Yval[0].equals("positionY")){
	    		Y = Float.parseFloat(Yval[1]);	 
	    	}else{
	    		PrintLog.DPrint("Error Yval[0] is not positionY");
	    	}
    	
	    	goToMouse(X,Y);
	    	
	    	
	}
	
	public static void clickHandler(String[] parts){
		float Z = 0;
		
    	String[] Zval = (parts[4].split(":")); 
    	
    	if(Zval[0].equals("positionZ")){
    		Z = Float.parseFloat(Zval[1]);	 
    	}else{PrintLog.DPrint("Error Zval[0] is not positionY");}	
    	
		if(Z < CLICK_THRESH){
    		if(currentTool > WHAT && isClicked == 0){
    			PrintLog.DPrint("CLICKEDDDDDD!@!!#!$");
    			clickMouse();
//    			if(currentTool == CROSSHAIR){
//    				releaseMouse();
//    				return;
//    			}
    			isClicked = 1;
    		}
    	}else {
    		if(isClicked == 1){
    			PrintLog.DPrint("UN-CLICKEDDDDDD!@!!#!$");
    			isClicked = 0;
    			releaseMouse();
    		}
    	}
	}
	
	// Assume that Leap Motion Usable Space is 500 x 500 square, subject to change. 
	private static void goToMouse(float Xa, float Y){
		PrintLog.DPrint("In goToMouse");
		try{
			
			if(Xa< -150){
				Xa = -150;
			}
			else if (Xa > 150){
				Xa = 150;
			}
			int X = (int) Math.abs(Xa);
			
			if(Xa >= 0){
				X += 150;
			}
			else if (Xa < 0){
				X = (int) (150+Xa);
			}
			if(Y >= 350){
				Y = 50;
			}
			if(Y < 50){
				Y = 350;
			}			
			
			Y = Y-50;
						
			Y = Y - USABLE_SPACE_Y;
			Y = Math.abs(Y);
			
			mouse.mouseMove((int)(xMultiplier*X),(int)(yMultiplier*Y));
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		}catch(Exception e){}
	}
	
	public static void switchTool(){
		currentTool++; 
		if(currentTool > WHAT){
			currentTool = SCROLL;
			switchMouseAction();
		}		
		else if(currentTool != PAN){
			switchMouseAction();
		}
		else if(currentTool == PAN){
			switchMouseAction();
			currentTool++;
			switchMouseAction();
		}
	}
	
	public static void switchToScroll(){
		currentTool = SCROLL;
		switchMouseAction();
	}
	
	public static void switchMouseAction(){
		EventManager eventManager;
		eventManager = EventManager.getInstance();
		
		String[] changeTo = {"rekt"};
		
		switch(currentTool){
		case SCROLL: 	changeTo[0] = "scroll";	
						break;
		case ZOOM:		changeTo[0] = "zoom";	
						break;
		case PAN: 		changeTo[0] = "pan";	
						break;
		case CROSSHAIR: changeTo[0] = "crosshair";	
						break;
		case WHAT:		changeTo[0] = "what";
						break;
		default: 		PrintLog.DPrint("Error in switchMouseAction : " + changeTo);
						break;
		}			
		
		
		try {
			eventManager.mouseLeftAction(changeTo);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		FourCorners.switchButton();
	}
	
	public static void clickMouse(){
		mouse.mousePress(InputEvent.BUTTON1_MASK);
	}
	
	public static void releaseMouse(){
		mouse.mouseRelease(InputEvent.BUTTON1_MASK);

	}
	
}