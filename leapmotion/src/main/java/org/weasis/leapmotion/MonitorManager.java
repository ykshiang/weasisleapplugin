package org.weasis.leapmotion;
/*
 * Copyright (c) 2012, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle or the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.geom.Ellipse2D;
import static java.awt.GraphicsDevice.WindowTranslucency.*;
import org.weasis.leapmotion.PrintLog;

public class MonitorManager extends JFrame {
	
	final private String SETTOSCROLL_BTN_MSG = "Set Tool To Scroll";
	final private String NEXTTOOL_BTN_MSG = "Next Tool";
	final private String PRESETS_BTN_MSG = "Presets";
	final private String RESET_BTN_MSG = "Display Reset";
	
	private JButton previous;
	private JButton next;
	private JButton presets;
	private JButton reset;
	
	public enum Panel {
		SETTOSCROLL,
	    NEXTTOOL,
	    PRESETS,
	    RESET,
	    NONE
	}
	
    public MonitorManager() {
        super("MonitorManager");
     
        JPanel firstPanel = new JPanel();
        firstPanel.setLayout(new GridLayout(2, 2));
        firstPanel.setMaximumSize(new Dimension(1200, 1200));
        
        previous = new JButton(SETTOSCROLL_BTN_MSG);
        previous.setFont(new Font("Arial", Font.PLAIN, 20));
        previous.setPreferredSize(new Dimension(300, 300));
        previous.setBorder(null);
        //previous.setHorizontalAlignment(SwingConstants.RIGHT);
        //previous.setVerticalAlignment(SwingConstants.BOTTOM);
        firstPanel.add(previous);
        
        next = new JButton(NEXTTOOL_BTN_MSG);
        next.setFont(new Font("Arial", Font.PLAIN, 20));
        next.setPreferredSize(new Dimension(300, 300));
        next.setBorder(null);
        //next.setHorizontalAlignment(SwingConstants.LEFT);
        //next.setVerticalAlignment(SwingConstants.BOTTOM);
        firstPanel.add(next);
        
        presets = new JButton(PRESETS_BTN_MSG);
        presets.setFont(new Font("Arial", Font.PLAIN, 20));
        presets.setPreferredSize(new Dimension(300, 300));
        presets.setBorder(null);
        //presets.setHorizontalAlignment(SwingConstants.RIGHT);
        //presets.setVerticalAlignment(SwingConstants.TOP);
        firstPanel.add(presets);
        
        reset = new JButton(RESET_BTN_MSG);
        reset.setFont(new Font("Arial", Font.PLAIN, 20));
        reset.setPreferredSize(new Dimension(300, 300));
        reset.setBorder(null);
        //reset.setHorizontalAlignment(SwingConstants.LEFT);
        //reset.setVerticalAlignment(SwingConstants.TOP);
        firstPanel.add(reset);
        
        // It is best practice to set the window's shape in
        // the componentResized method.  Then, if the window
        // changes size, the shape will be correctly recalculated.
        addComponentListener(new ComponentAdapter() {
            // Give the window an elliptical shape.
            // If the window is resized, the shape is recalculated here.
            @Override
            public void componentResized(ComponentEvent e) {
                setShape(new Ellipse2D.Double(0,0,getWidth(),getHeight()));
            }
        });
        
        setUndecorated(true);
        setSize(500,500);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        add(firstPanel);
  
    }
    
    public void resetAll(){
    	previous.setBackground(null);
    	//previous.setOpaque(false);
    	
    	next.setBackground(null);
    	//next.setOpaque(false);
    	
    	presets.setBackground(null);
    	//presets.setOpaque(false);
    	
    	reset.setBackground(null);
    	//reset.setOpaque(false);
    }
    
    public void activatePanel(Panel panel){
    	resetAll();
		if(panel == Panel.SETTOSCROLL){
			PrintLog.DPrint("Activating SET TO SCROLL Button");
    		previous.setOpaque(true);
    		previous.setBackground(Color.GREEN);
		}else if(panel == Panel.NEXTTOOL){
			PrintLog.DPrint("Activating NEXT TOOL Button");
    		next.setOpaque(true);
    		next.setBackground(Color.GREEN);
		}else if(panel == Panel.PRESETS){
			PrintLog.DPrint("Activating PRESETS Button");
    		presets.setOpaque(true);
    		presets.setBackground(Color.GREEN);
		}else if(panel == Panel.RESET){
			PrintLog.DPrint("Activating RESET Button");
    		reset.setOpaque(true);
    		reset.setBackground(Color.GREEN);
		}else if(panel == Panel.NONE){
			return;
		}
    }
    
    
}