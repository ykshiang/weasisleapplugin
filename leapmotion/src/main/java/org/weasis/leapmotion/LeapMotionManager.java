package org.weasis.leapmotion;
import static java.awt.GraphicsDevice.WindowTranslucency.PERPIXEL_TRANSPARENT;
import static java.awt.GraphicsDevice.WindowTranslucency.TRANSLUCENT;

import java.awt.AWTException;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.KeyboardFocusManager;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.JMenu;
import javax.swing.KeyStroke;
import org.weasis.core.api.gui.util.RadioMenuItem;
import org.weasis.dicom.codec.display.PresetWindowLevel;
import org.weasis.dicom.viewer2d.EventManager;
import java.io.IOException;

import org.weasis.leapmotion.MonitorManager;
import org.weasis.leapmotion.MonitorManager.Panel;
import org.weasis.leapmotion.motion.CircularMotion;
import org.weasis.leapmotion.SampleToolBar;
// Motion Types

public class LeapMotionManager implements ActionListener, Observer {
	
	private final EventManager eventManager;
	private final FourCorners fourCornerManager;
	private final CommandParser commandParser;
	private final MouseMovement mouseMovement;
    static KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
    
    /**
     * establishConnection creates a ChatSocketClient instance and bind the 
     * ChatSocketClient's observable to LeapMotionManager.
     */
    private void establishConnection(){
    	ChatSocketClient myChatClient = new ChatSocketClient();
    	myChatClient.addObserver(this);
    }
    
    /**
     * Constructor method. It obtains the EventManager Singleton so that 
     * it can be used in this class. Also, it creates the centre monitor. 
     */
	public LeapMotionManager() {
		establishConnection();
		fourCornerManager = new FourCorners();
		eventManager = EventManager.getInstance();	
		commandParser = new CommandParser(fourCornerManager, eventManager);
		mouseMovement = new MouseMovement();
		
	 }
	
	/**
	 * update is triggered when ChatSocketClient receives data from the Server.
	 * Within "update" is a command parser which parses information received from
	 * the Server.  
	 * @param o Observable Object
	 * @param arg Arguments passed from the observable object.
	 */
	@Override
	public void update(Observable o, Object arg) {
		//manager.focusNextComponent();
		CommandParser.parseArgs(arg);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	public static void switchToExplorer(){
    	click(100,300);    	
    }
  
    public static boolean checkFocusExplorer() throws Exception{
		String A = (manager.getFocusOwner().toString());
		PrintLog.DPrint(A);
		if(A.contains("SeriesThumbnail"))
		{
			return true;
		}
		else{ 
			return false;
		}
		
    }
    
    public static void click(int x, int y){
    	try{
	        Robot bot = new Robot();
	        bot.mouseMove(x, y);    
	        bot.mousePress(InputEvent.BUTTON1_MASK);
	        bot.mouseRelease(InputEvent.BUTTON1_MASK);
    	}
    	catch(AWTException e){
    		
    	}
    }
}