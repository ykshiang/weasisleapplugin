package org.weasis.leapmotion;

import static java.awt.GraphicsDevice.WindowTranslucency.PERPIXEL_TRANSPARENT;
import static java.awt.GraphicsDevice.WindowTranslucency.TRANSLUCENT;

import java.awt.AWTException;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.lang.Math;

import org.weasis.dicom.viewer2d.EventManager;
import org.weasis.dicom.viewer2d.ResetTools;
import org.weasis.leapmotion.MonitorManager;
import org.weasis.leapmotion.MonitorManager.Panel;
import org.weasis.leapmotion.MouseMovement;

public class FourCorners {
	private final EventManager eventManager;
	public static MonitorManager monitorManager;

	private static int currentPreset = -1;
	private static int delay = 0;
	private static int currentState = 0;
	private static long currentTime = 0;
	private static boolean doOnce = false;
	private static final int MAX_DELAY = 0;
	private static final int CIRCLE_RADIUS = 50;


	private boolean isWithinInBound(float X, float Y) {
		double distance = Math.sqrt(Math.pow(X, 2) + Math.pow(Y, 2));
		return (distance < CIRCLE_RADIUS);
	}

	/**
	 * showMonitor activates the centre monitor.
	 */
	public void showMonitor() {
		monitorManager.setVisible(true);
		monitorManager.toFront();
		monitorManager.repaint();
		currentTime = System.currentTimeMillis();		
		doOnce = false;
	}

	/**
	 * hideMonitor deactivates the centre monitor.
	 */
	public void hideMonitor() {
		long now = System.currentTimeMillis();
		if(now-currentTime > 100 && !doOnce){
			System.out.println((System.currentTimeMillis() + " : " + currentTime));
			monitorManager.setVisible(false);
			doOnce = true;
			MouseMovement.switchMouseAction();
			doAction();
		}
	}

	public FourCorners() {
		delay = 0;
		eventManager = EventManager.getInstance();
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice gd = ge.getDefaultScreenDevice();
		final boolean isTranslucencySupported = gd.isWindowTranslucencySupported(TRANSLUCENT);

		// If shaped windows aren't supported, exit.
		if (!gd.isWindowTranslucencySupported(PERPIXEL_TRANSPARENT)) {
			System.err.println("Shaped windows are not supported");
			System.exit(0);
		}

		// If translucent windows aren't supported,
		// create an opaque window.
		if (!isTranslucencySupported) {
			System.out.println("Translucency is not supported, creating an opaque window");
		}

		monitorManager = new MonitorManager();

		// Set the window to 50% translucency, if supported.
		if (isTranslucencySupported) {
			monitorManager.setOpacity(0.8f);
		}
	}

	public void PredfinedHandler(String[] parts) {
		PrintLog.DPrint("PredefinedHandler!!!");
		String[] Xval = (parts[2].split(":"));
		String[] Yval = (parts[3].split(":"));
		float X = 0, Y = 0;

		if (Xval[0].equals("positionX")) {
			X = Float.parseFloat(Xval[1]);
		} else {
			PrintLog.DPrint("Error Xval[0] is not positionX");
		}

		if (Yval[0].equals("positionY")) {
			Y = Float.parseFloat(Yval[1]);
		} else {
			PrintLog.DPrint("Error Yval[0] is not positionY");
		}
		//MouseMovement.MouseHandler(parts);
		
		SwitchToCorner(X,Y);
	}

	private synchronized void SwitchToCorner(float X, float Y) {
		showMonitor();

		// if(this.isWithinInBound(X, Y) == false){
		if (X > 0 && Y > 250) {
			monitorManager.activatePanel(Panel.NEXTTOOL);
			currentState = 1;
		} else if (X > 0 && Y < 250) {
			monitorManager.activatePanel(Panel.RESET);
			currentState = 2;
		} else if (X < 0 && Y > 250) {
			monitorManager.activatePanel(Panel.SETTOSCROLL);
			currentState = 3;
		} else if (X < 0 && Y < 250) {
			monitorManager.activatePanel(Panel.PRESETS);
			currentState = 4;
		}else{
			//monitorManager.activatePanel(null);
			currentState = 0;
		}

		return;
	}
	
	private void doAction(){
		switch(currentState){
			case 1:		MouseMovement.switchTool();
						System.out.println("Tool Switched To :" + MouseMovement.currentTool);
						break;
			case 2:		eventManager.resetDisplay();
						switchButton();
						System.out.println("Display Reset");
						break;
			case 3: 	MouseMovement.switchToScroll();
						System.out.println("Tool Switched To Scroll");
						break;
			case 4:		if (currentPreset + 1 > 3) {
							currentPreset = 0;
						} else {
							currentPreset++;
						}
						try {
							try {
								Thread.sleep(200);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						doPreset();
						
						System.out.println("Preset # : " + currentPreset + " now Active!");
						break;
			default : return;
		}
	}
	
	private void doPreset(){
		try {
			Robot robot = new Robot();
		switch(currentPreset){
		case 0:   	robot.keyPress(KeyEvent.VK_3);
		      	  	robot.keyRelease(KeyEvent.VK_3);			   
		      	  	break;
		case 1: 	robot.keyPress(KeyEvent.VK_4);
    	  			robot.keyRelease(KeyEvent.VK_4);			   
    	  			break;
		case 2: 	robot.keyPress(KeyEvent.VK_5);
    	  			robot.keyRelease(KeyEvent.VK_5);			   
    	  			break;
		case 3:		robot.keyPress(KeyEvent.VK_7);
    	  			robot.keyRelease(KeyEvent.VK_7);			   
    	  			break;
		default : break;
		}
		} catch (AWTException e) {
		    e.printStackTrace();
	  }
	}
	public static void switchButton(){
		switch(MouseMovement.currentTool){
			case 0:     SampleToolBar.changeToolsToScroll();
						break;
			case 1:		SampleToolBar.changeToolsToZoom();
						break;
			case 2:		break;
			case 3:		SampleToolBar.changeToolsToPan();
						break;
			case 4:		SampleToolBar.changeToolsToCrosshair();
						break;
			
			default : return;
		}
	}
}
