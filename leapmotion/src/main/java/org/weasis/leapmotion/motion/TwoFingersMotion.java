package org.weasis.leapmotion.motion;

import java.io.IOException;

import org.weasis.dicom.viewer2d.EventManager;

public class TwoFingersMotion {
	private final EventManager eventManager;
	public TwoFingersMotion(EventManager arg){
		eventManager = arg;
	}
	
	/**
     * MotionTwoFing command is triggered when Command Parser receives a Two Fingers 
     * command from the server. 
     * @param parts
     */
    public void execute(String[] parts){
    	System.out.println("Two Finger!!!");
    	String[] XpartType = (parts[2].split(":"));
    	String[] YpartType = (parts[3].split(":"));
    	//System.out.println(partType[0] + " " + partType[1]);
    	if(XpartType[0].equals("positionX")){
    		System.out.println("In Two");
    		float XcurrentTwoFing=Float.parseFloat(XpartType[1]);
    		final int Xmultiplier = (Math.abs((int)XcurrentTwoFing)-48)/10;
    		String mult = Integer.toString(Xmultiplier);
    		if(XcurrentTwoFing > 50){
    			String[] test = {"--", mult, "0"};	
    			try{
	    			eventManager.wl(test);
	    		}catch(IOException e){
	    			//PrintLog.EPrint("IO Exception");
	    		}
    		}
    		
    		else if (XcurrentTwoFing < -50) {
    			String[] test = {"--", "-"+mult, "0"};	
    			try{
	    			eventManager.wl(test);
	    		}catch(IOException e){
	    			//PrintLog.EPrint("IO Exception");
	    		}
    					
    		}
    	//	previousTwoFing = currentTwoFing;
    	}   
    	
    	if(YpartType[0].equals("positionY")){
    		System.out.println("In Two");
    		float YcurrentTwoFing=Float.parseFloat(YpartType[1]);
    		if(YcurrentTwoFing > 300){
    			final int Ymultiplier = Math.abs((int)(YcurrentTwoFing-300)/15);
        		String mult = Integer.toString(Ymultiplier);
    			String[] test = {"--", "0", mult};	
    			try{
	    			eventManager.wl(test);
	    		}catch(IOException e){
	    			//PrintLog.EPrint("IO Exception");
	    		}
    		}
    		
    		else if (YcurrentTwoFing < 100){
    			final int Ymultiplier = Math.abs((int)YcurrentTwoFing+100)/15;
        		String mult = Integer.toString(Ymultiplier);
    			String[] test = {"--", "0", "-"+mult};	
    			try{
	    			eventManager.wl(test);
	    		}catch(IOException e){
	    			//PrintLog.EPrint("IO Exception");
	    		}
    					
    		}
    	//	previousTwoFing = currentTwoFing;
    	}
    	
    }
}
