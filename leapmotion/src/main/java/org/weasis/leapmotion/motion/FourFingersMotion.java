package org.weasis.leapmotion.motion;

import org.weasis.dicom.viewer2d.EventManager;
import org.weasis.leapmotion.FourCorners;

public class FourFingersMotion {
	private final EventManager eventManager;
	private final FourCorners fourCornerManager;
	public FourFingersMotion(EventManager arg1, FourCorners arg2){
		eventManager = arg1;
		fourCornerManager = arg2;
	}
	
    public void execute(String[] parts){
    	fourCornerManager.PredfinedHandler(parts);	
    }
}
