package org.weasis.leapmotion.motion;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import org.weasis.dicom.viewer2d.EventManager;
import org.weasis.leapmotion.PrintLog;
import org.weasis.leapmotion.IntTuple;
import org.weasis.leapmotion.MouseMovement;

public class CircularMotion {
	
	private static int lagCount = 0;
	private static int threshold = 5;
	private static final int minRadius = 15;
	private static final int maxRadius = 50;
	// maxThreshold represents the sensitivity of the gesture.
	// The higher the threshold the less sensitive it is.
	private static int maxThreshold = 13;
	
	public enum Window {
	    EXPLORER,
	    VIEWER,
	}
	
	private static void setMaxThreshold(boolean ifExplorer){
		if(ifExplorer){
			maxThreshold = 25;
		}else{
			maxThreshold = 13;
		}
	}
	
	public static void MotionHandler(String[] parts, boolean ifExplorer, Window win){
		setMaxThreshold(ifExplorer);
		String[] radiusKV = (parts[3].split(":"));
		String radiusString = radiusKV[1];
    	IntTuple params = convertRadiusToSpeed(radiusString);
    	if(ifExplorer){
    		params = convertRadiusToExplorerSpeed(radiusString);
    	}
    	int speed = params.y;
    	String pageSkipped = Integer.toString(params.x);
    	
    	adjustThreshold(speed);
		if(lagCount >= threshold){
			EventManager eventManager = EventManager.getInstance();
	    	String[] partType = (parts[2].split(":"));
	    	PrintLog.DPrint("In Circular");
	    	// Assumption : Don't check for ciruclar since if sending clockwise we know its clockwise.
	    	if(partType[0].equals("clockwiseness")){
		    	if(partType[1].equals("clockwise")){
		    		if(!ifExplorer){
		    			if(MouseMovement.currentTool == MouseMovement.SCROLL){
				    		String[] test = {"-i", pageSkipped};
				    		try{
				    			eventManager.scroll(test);
				    		}catch(IOException e){
				    			PrintLog.EPrint("IO Exception");
				    		}
		    			}else if(MouseMovement.currentTool == MouseMovement.ZOOM){
		    				String[] test = {"-i", pageSkipped};
				    		try{
				    			eventManager.zoom(test);
				    		}catch(IOException e){
				    			PrintLog.EPrint("IO Exception");
				    		}
		    			}else {/* Do Nothing */}
		    		}
		    		else{
		    			try {
	    				      Robot robot = new Robot();
	    				      // Simulate a key press
	    				      robot.keyPress(KeyEvent.VK_DOWN);
	    				      robot.keyRelease(KeyEvent.VK_DOWN);
	    				} catch (AWTException e) {
	    				    e.printStackTrace();
	    				}		    			
		    		}
		    	}
		    	else if ( partType[1].equals("counterclockwise")){
		    		if(!ifExplorer){
		    			if(MouseMovement.currentTool == MouseMovement.SCROLL){
				    		String[] test = {"-d", pageSkipped};
				    		try{
				    			eventManager.scroll(test);
				    		}catch(IOException e){
				    			PrintLog.EPrint("IO Exception");
				    		}
		    			}else if(MouseMovement.currentTool == MouseMovement.ZOOM){
		    				String[] test = {"-d", pageSkipped};
				    		try{
				    			eventManager.zoom(test);
				    		}catch(IOException e){
				    			PrintLog.EPrint("IO Exception");
				    		}
		    			}else {/* Do Nothing */}
		    		}else{
		    			try {
		    				      Robot robot = new Robot();
		    				      // Simulate a key press
		    				      robot.keyPress(KeyEvent.VK_UP);
		    				      robot.keyRelease(KeyEvent.VK_UP);
		    				} catch (AWTException e) {
		    				    e.printStackTrace();
		    				}
		    		}
		    	}    	
	    	}
	    	System.out.println(lagCount);
	    	lagCount = 0;
		}else{
			lagCount++;
		}
    }
	/**
     * This method sets the threshold value to the speed. 
     * @param speed: integer value to be stored at the threshold
     */
	private static void adjustThreshold(int speed){
		threshold = speed;
	}
	/**
     * This method computes the speed and  
     */
	private static IntTuple convertRadiusToSpeed(String value){
		float radius = Float.parseFloat(value);
		
		if(radius>maxRadius){
			if(radius < maxRadius + 20){
				radius = maxRadius;
			}else if(radius >= maxRadius + 20 && radius < maxRadius + 40){
				IntTuple rslt = new IntTuple(5, 0);
				return rslt;
			}else if(radius >= maxRadius + 40 && radius < maxRadius + 60){
				IntTuple rslt = new IntTuple(10, 0);
				return rslt;
			}else if(radius >= maxRadius + 60){
				IntTuple rslt = new IntTuple(15, 0);
				return rslt;
			}
		}else if(radius < minRadius){
			radius = minRadius;
		}
		radius = radius - minRadius;
		float thresholdVal = (maxThreshold - (radius/(maxRadius - minRadius))*maxThreshold);
		IntTuple rslt = new IntTuple(1, (int)thresholdVal);
		return rslt;
	}
	
	private static IntTuple convertRadiusToExplorerSpeed(String value){
		float radius = Float.parseFloat(value);
		
//		if(radius>maxRadius){
//			radius = maxRadius;
//		}else if(radius < minRadius){
			radius = minRadius;
		//}
		radius = radius - minRadius;
		float thresholdVal = (maxThreshold - (radius/(maxRadius - minRadius))*maxThreshold);
		IntTuple rslt = new IntTuple(1, (int)thresholdVal);
		return rslt;
	}
}
