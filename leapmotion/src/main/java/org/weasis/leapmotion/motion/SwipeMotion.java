package org.weasis.leapmotion.motion;

import javax.swing.JMenu;

import org.weasis.core.api.gui.util.RadioMenuItem;
import org.weasis.dicom.codec.display.PresetWindowLevel;
import org.weasis.dicom.viewer2d.EventManager;

public class SwipeMotion {
	private final EventManager eventManager;
	public SwipeMotion(EventManager arg){
		eventManager = arg;
	}
	
	/**
     * MotionSwipe command is triggered when Command Parser receives a swipe command 
     * from the server. 
     * @param parts Command arguments passed in from the server. 
     * 
     */
    public void execute(String[] parts){
    	System.out.println("In Swipe!!!");
    	JMenu menu = eventManager.getPresetMenu("weasis.contextmenu.presets");
    	int[] presetList = null; 
    	
        
        for (java.awt.Component mitem : menu.getMenuComponents()) {
            RadioMenuItem ritem = ((RadioMenuItem) mitem);
            PresetWindowLevel preset = (PresetWindowLevel) ritem.getUserObject();
        	System.out.println("123456" + preset);
            System.out.println("This is the preset keycode below!");
            System.out.println(preset.getKeyCode());
            presetList[preset.getKeyCode()] = preset.getKeyCode();
        }
        
        for(int i : presetList){
        	System.out.println((char)i);
        }
    	
    	//System.out.println(partType[0] + " " + partType[1]);
    }
}
