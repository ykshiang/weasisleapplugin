package org.weasis.leapmotion;

import java.util.Hashtable;
import org.weasis.leapmotion.LeapMotionManager;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.weasis.core.api.gui.Insertable;
import org.weasis.core.api.gui.Insertable.Type;
import org.weasis.core.api.gui.InsertableFactory;


@Component(immediate = false)
@Service
@Property(name = "org.weasis.dicom.viewer2d.View2dContainer", value = "true")
public class SampleToolFactory implements InsertableFactory{
    private static final Logger LOGGER = LoggerFactory.getLogger(SampleToolFactory.class);  
    private LeapTool toolPane = null;
    @Override
    public Type getType() {
        return Type.TOOL;
    }

    @Override
    public Insertable createInstance(Hashtable<String, Object> properties) {
        if (toolPane == null) {
            toolPane = new LeapTool(getType());
        }
        return toolPane;
    }

    @Override
    public void dispose(Insertable tool) {
        if (toolPane != null) {
            toolPane = null;
        }
    }

    @Override
    public boolean isComponentCreatedByThisFactory(Insertable tool) {
        return tool instanceof LeapTool;
    }

    @Activate
    protected void activate(ComponentContext context) {
        LOGGER.info("Activate the Sample panel");
        LeapMotionManager leapManager = new LeapMotionManager();
    }

    @Deactivate
    protected void deactivate(ComponentContext context) {
        LOGGER.info("Deactivate the Sample panel");
    }
}