package org.weasis.leapmotion;
import java.util.Date;
import java.sql.Timestamp;

public class PrintLog {
	private static Date date = new java.util.Date();
	
	public static void DPrint(String message){
		if(CommandParser.verbose){
			Timestamp time = new Timestamp(date.getTime());
			System.out.println(time + " *LEAP DEBUG* -- " + message);
		}
	}
	
	public static void EPrint(String message){
		if(CommandParser.verbose){
			Timestamp time = new Timestamp(date.getTime());
			System.out.println(time + " *LEAP ERROR* -- " + message);
		}
	}
	
	public static void LPrint(String message){
		if(CommandParser.verbose){
			Timestamp time = new Timestamp(date.getTime());
			System.out.println(time + " *LEAP LOG* -- " + message);
		}
	}
}
