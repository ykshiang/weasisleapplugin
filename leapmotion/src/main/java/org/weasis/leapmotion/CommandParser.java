package org.weasis.leapmotion;

import java.awt.AWTException;
import java.awt.KeyboardFocusManager;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;

import javax.swing.JMenu;

import org.weasis.core.api.gui.util.RadioMenuItem;
import org.weasis.dicom.codec.display.PresetWindowLevel;
import org.weasis.dicom.viewer2d.EventManager;
import org.weasis.leapmotion.motion.CircularMotion;
import org.weasis.leapmotion.motion.CircularMotion.Window;
import org.weasis.leapmotion.motion.*;

public class CommandParser {
	
	private static FourCorners fourCornerManager;
	private static EventManager eventManager;
	private static CircularMotion circularMotion;
	private static TwoFingersMotion twoFingersMotion;
	private static ThreeFingersMotion threeFingersMotion;
	private static FourFingersMotion fourFingersMotion;
	private static FiveFingersMotion fiveFingersMotion;
	private static SwipeMotion swipeMotion;
	private static int sensitivity = 10;
	public static boolean verbose = true;
	private final static int SKIP_CONSTANT = 7;
	private static boolean stayGreen = false; 
	private static long greenTimer = 0; 
	
	public CommandParser(FourCorners arg1, EventManager arg2){
		fourCornerManager = arg1;
		eventManager = arg2;
		circularMotion = new CircularMotion();
		twoFingersMotion = new TwoFingersMotion(eventManager);
		threeFingersMotion = new ThreeFingersMotion(eventManager);
		fourFingersMotion = new FourFingersMotion(eventManager, fourCornerManager);
		fiveFingersMotion = new FiveFingersMotion(eventManager);
		swipeMotion = new SwipeMotion(eventManager);
		
	}
	
	private static void handleCircular(String[] parts) throws Exception{
		fourCornerManager.hideMonitor();
		CircularMotion.MotionHandler(parts, LeapMotionManager.checkFocusExplorer(), Window.VIEWER);
	}
	
	private static void handle1Finger(String[] parts) throws Exception{
		SampleToolBar.changeHandsToOne();
		MouseMovement.MouseHandler(parts);
		MouseMovement.clickHandler(parts);
		
	}
	
	private static void handle2Finger(String[] parts) throws Exception{
		SampleToolBar.changeHandsToTwo();
		fourCornerManager.hideMonitor();
		twoFingersMotion.execute(parts);
	}
	
	private static void handle3Finger(String[] parts) throws Exception{
		SampleToolBar.changeHandsToThree();
		fourCornerManager.hideMonitor();
		threeFingersMotion.execute(parts);
	}
	
	private static void handle4Finger(String[] parts) throws Exception{
		SampleToolBar.changeHandsToFour();
		fourFingersMotion.execute(parts);
	}
	
	private static void handleSwipe(String[] parts) throws Exception{
		fourCornerManager.hideMonitor();
		swipeMotion.execute(parts);
	}
	
	private static void handle5Finger(String[] parts) throws Exception{
		SampleToolBar.changeHandsToFive();
		fourCornerManager.hideMonitor();
	}
	
	private static void handleTap(String[] parts) throws Exception{
		MouseMovement.switchTool();
	}
	
	private static void handleUnRecognized(Object arg) throws Exception{
		
		PrintLog.DPrint("Error from server -- " + (String)arg);
		fourCornerManager.hideMonitor();
		System.out.println(greenTimer);
		System.out.println(System.currentTimeMillis());
		System.out.println(System.currentTimeMillis() - greenTimer);
		if(System.currentTimeMillis() - greenTimer > 2000){
			SampleToolBar.changeHandsToNo();
			SampleToolBar.changeStatusToRed();
		}
	}
	
	/**
	 * Command Parser parses command and invokes commands accordingly.
	 * @param arg
	 */
	public static void parseArgs(Object arg){
		try{
	    	String[] parts = ((String)arg).split(";");
	    	PrintLog.DPrint((String)arg);
	    	if(parts.length >= 2){
	    		String[] partType = (parts[1].split(":"));
		    	if(partType[0].equals("motionType")){
		    		//System.out.println("Update called with Arguments: "+parts[0]);
		    		//System.out.println(parts[2]+";" +parts[3]);
		    		SampleToolBar.changeStatusToGreen();
		    		greenTimer = System.currentTimeMillis();
		    		//String[] partType = (parts[0].split(":"));
		    		if(partType[1].equals("circular")){
		    			handleCircular(parts);
		    		}
		    		else if (partType[1].equals("1fingers")){
		    			handle1Finger(parts);
		    		}
		    		else if (partType[1].equals("2fingers")){
		    			handle2Finger(parts);		    			
		    		}
					else if (partType[1].equals("3fingers")){
						handle3Finger(parts);
					}
					else if (partType[1].equals("4fingers")){
						handle4Finger(parts);
					}
					else if (partType[1].equals("swipe")){
						handleSwipe(parts);
					}
					else if(partType[1].equals("5fingers")){
						handle5Finger(parts);
					}
					else if(partType[1].equals("tap")){
						handleTap(parts);
					}
		    	}
	    	}else if(parts.length<2){
	    			handleUnRecognized(arg);
	    	}
    	}
    	catch(Exception e){
    		//PrintLog.DPrint(e.toString());
    	}
	}
}
