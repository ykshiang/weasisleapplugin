package org.weasis.dicom.viewer2d;
import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.net.InetAddress;
import java.util.Observable;

class ChatSocketClient extends Observable {
    private Socket socket = null;
    private InputStream inStream = null;
    private OutputStream outStream = null;
    private InetAddress ip_address = null;
    private int port = 0;
    private String message;

    public ChatSocketClient(String ip, String port) {
        try {
            this.ip_address = InetAddress.getByName("206.87.125.224");
            this.port = Integer.parseInt("1078");
            this.createSocket();
        }
        catch (UnknownHostException e){
            System.out.println("IP cannot be found!");
        }
    }


    public void receivedMessage(String value) {
        // mark as value changed
        setChanged();
        // trigger notification
        notifyObservers(value);
    }

    public void createSocket() {
        try {
            socket = new Socket(this.ip_address, this.port);
            System.out.println("Connected");
            inStream = socket.getInputStream();
            outStream = socket.getOutputStream();
            createReadThread();
            createWriteThread();
        } catch (UnknownHostException u) {
            u.printStackTrace();
        } catch (IOException io) {
            io.printStackTrace();
        }
    }

    public void createReadThread() {
        Thread readThread = new Thread() {
            public void run() {
                while (socket.isConnected()) {

                    try {
                        byte[] readBuffer = new byte[200];
                        int num = inStream.read(readBuffer);

                        if (num > 0) {
                            byte[] arrayBytes = new byte[num];
                            System.arraycopy(readBuffer, 0, arrayBytes, 0, num);
                            String recvedMessage = new String(arrayBytes, "UTF-8");
                            receivedMessage(recvedMessage);
                        }/* else {
                            // notify();
                        }*/
                        ;
                        //System.arraycopy();
                    }catch (SocketException se){
                        System.exit(0);
                    } catch (IOException i) {
                        i.printStackTrace();
                    }
                }
            }
        };
        readThread.setPriority(Thread.MAX_PRIORITY);
        readThread.start();
    }

    public void createWriteThread() {
        Thread writeThread = new Thread() {
            public void run() {
                while (socket.isConnected()) {
                    try {
                        BufferedReader inputReader = new BufferedReader(new InputStreamReader(System.in));
                        sleep(100);
                        String typedMessage = inputReader.readLine();
                        if (typedMessage != null && typedMessage.length() > 0) {
                            synchronized (socket) {
                                outStream.write(typedMessage.getBytes("UTF-8"));
                                sleep(100);
                            }
                        }
                        ;
                        //System.arraycopy();
                    } catch (IOException i) {
                        i.printStackTrace();
                    } catch (InterruptedException ie) {
                        ie.printStackTrace();
                    }
                }
            }
        };
        writeThread.setPriority(Thread.MAX_PRIORITY);
        writeThread.start();
    }
}